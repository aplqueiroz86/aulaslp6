<!-- Card -->
<div class="card mt-5">

  <!-- Card image -->
  <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/<?= $img ?>.jpg" alt="Card image cap">

  <!-- Card content -->
  <div class="card-body">

    <!-- Title -->
    <h4 class="card-title"><a><?= $title ?></a></h4>
    <!-- Text -->
    <p class="card-text"><?= $descr ?></p>
    <!-- Button -->
    <a href="#" class="btn btn-primary"><?= $label ?></a>
  
  </div>

</div>
<!-- Card -->