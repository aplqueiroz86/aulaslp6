<div class="container">
    <?= validation_errors('<div class="alert alert-danger">','</div>')?>;
    <div class="card">
        <div class="card-header"><h4>Dados Pessoais</h4></div>
            <div class="card-body">
                <form method="POST" class="text-center border border-light p-5">
                    <p class="h4 mb-4">Cadastre-se</p>
                    <div class="form-row mb-4">
                        <div class="col">
                            <!-- First name -->
                            <input type="text" name="nome" value="<?= set_value('nome') ?>" class="form-control" placeholder="Nome">
                        </div>
                        <div class="col">
                            <!-- Last name -->
                            <input type="text" name="sobrenome" value="<?= set_value('sobrenome') ?>" class="form-control" placeholder="Sobrenome">
                        </div>
                    </div>
                    <div class="form-row mb-4">
                        <div class="col">
                            <input type="email" name="email" value="<?= set_value('email') ?>" class="form-control mb-4" placeholder="E-mail">
                        </div>
                        <div class="col">
                            <input type="password" name="senha" value="<?= set_value('senha') ?>" class="form-control" placeholder="Senha" aria-describedby="defaultRegisterFormPasswordHelpBlock">
                        </div>
                        <div class="col">
                            <input type="text" name="nascimento" value="<?= set_value('nascimento') ?>" class="form-control" placeholder="Nascimento" aria-describedby="defaultRegisterFormPhoneHelpBlock">
                        </div>
                    </div>
                    
                
            </div>
    </div>
</div>