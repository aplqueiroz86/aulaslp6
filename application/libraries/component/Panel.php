<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Panel{
        
        private $titulo;
        private $subtitulo;
        private $conteudo;
        private $link1;
        private $link2;
        private $colunas = 2;

        function __construct($row){
            $this->titulo  = $row->titulo;
            $this->subtitulo  = $row->subtitulo;
            $this->conteudo = $row->conteudo;
            $this->link1  = $row->link1;
            $this->link2  = $row->link2;
        }
        
        
        public function getHTML(){
            $html = '
            <div class="col-md-'.$this->colunas.' px-1 py-1">
                '.$this->getCard().'
            </div>
            ';
            
            return $html;
        }

        private function getCard(){
            $html = '
            <div class="card">
                '.$this->cardBody().'
            </div>
            ';
            return $html;

        }

        private function cardBody(){
            $html = '
            <div class="card-body bg-'.$this->color.'">
                <h5 class="card-title">'.$this->titulo.'</h5>
                <h6 class="card-subtitle mb-2 text-muted">'.$this->subtitulo.'</h6>
                <p class="card-text">'.$this->conteudo.'</p>
                <a href="#!" class="card-link">'.$this->link1.'</a>
                <a href="#!" class="card-link">'.$this->link2.'</a>
            </div>
            ';
            return $html;
        }

        public function setCols($num){
            $this->colunas = $num;

        }

        private $color;
        public function setColor($color){
            $this->color = $color;
        }
        
    }
    

?>