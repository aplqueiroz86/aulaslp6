<?php
include_once 'DAO.php';

class Endereco extends DAO{
    function __construct()
    {
        parent::__construct('endereco');
    }
    
    //sobrescrita de método: criar na classe 
    //filha um método que tem o mesmo nome de outros definido na classe pai
    //para especializaar algum comportamento
    
    public function salvar($data, $id_pessoa = 0){
        $data['id_pessoa'] = $id_pessoa;
        parent::salvar($data);
    }
}


?>