<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    include APPPATH.'libraries/component/Panel.php';
    include APPPATH.'libraries/component/Table.php';
    include_once APPPATH.'libraries/User.php';

    class ComponentModel extends CI_Model{
        private $color = array('primary','secondary', 'warning', 'danger','info','light', 'dark','success');
        
        public function getPanelList(){
            $rs = $this->db->get('panel_data', 10);
            $v = $rs->result();
            $html = '';

            foreach($v as $row){
                /*$titulo = $row->titulo;
                $subtitulo = $row->subtitulo;
                $conteudo = $row->conteudo;
                $link1 = $row->link1;
                $link2 = $row->link2;
                $panel = new Panel($titulo, $subtitulo, $conteudo, $link1, $link2);*/ 
                $panel = new Panel($row);
                $panel->setCols(3);
                $panel->setColor($this->color[rand(0, 7)]);
                $html .= $panel->getHTML();
            }


            return $html;

        }

        public function getTable(){
            $user = new User();
            $data = $user->getAll();
            $header = array('', 'Nome', 'Sobrenome', 'E-mail', 'Telefone', 'Senha', 'Criado em:');

            $table = new Table($data, $header);
            $table->set_header_color('elegant-color');
            $table->use_white_text();
            $table->zebra_table();
            //$table->small_table();
            $table->use_border();
            $table->use_hover();
            //$table->column_size(7);
            $table->mt(5);
            $table->use_action_button();
            return $table->getHTML();
        }
    }

    
?>