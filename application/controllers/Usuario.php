<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 class Usuario extends CI_Controller{
     public function index(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
       
        $this->load->model('UsuarioModel', 'model');
        $v['lista'] = $this->model->lista();
        $this->load->view('usuario/table_view', $v);

        $this->load->view('common/footer');
     }

    public function cadastro(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('UsuarioModel', 'model');
        $this->model->criar();
        $data['titulo'] = "Cadastro"; 
        $data['acao'] = "Enviar"; 
        $this->load->view('usuario/form_cadastro', $data);
        $this->load->view('common/footer');
    }

    
    public function edit($id){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('UsuarioModel', 'model');
        $this->model->atualizar($id);

        $data['titulo'] = "Edição do usuario"; 
        $data['acao'] = "Atualizar"; 
        $data['user'] = $this->model->carrega_usuario($id);

        print_r($data['user']);


        $this->load->view('usuario/form_cadastro', $data);
        $this->load->view('common/footer');
    }

    public function delete($id){
        $this->load->model('UsuarioModel', 'model');
        $this->model->delete($id);
        redirect('usuario');
    }
 }

?>