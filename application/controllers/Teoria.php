<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teoria extends CI_Controller{
    public function index(){
        echo "Um pouco da teoria relacionada a POO";

    }

    public function heranca(){
        $this->load->model('TeoriaModel','teoria');
        $this->teoria->heranca();
    }

    public function polimorfismo(){
        echo "Polimorfismo : um objeto assumindo várias formas";
        $this->load->model('TeoriaModel','teoria');
        $this->teoria->polimorfismo();
    }
}

?>